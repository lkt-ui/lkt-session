export declare const setCookie: (name: string, value: string, expiresInSeconds?: number) => void;
export declare const getCookie: (name: string) => string;
export declare const removeCookie: (name: string) => void;
